
function initMap() {
// Create a map object and specify the DOM element for display.
var myLatLng = {lat: 7.101964, lng: 125.634532};

var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 17,
  center: myLatLng
});

var marker = new google.maps.Marker({
  position: myLatLng,
  map: map,
  title: 'Pistacia'
});

}


$( document ).ready(function() {
	var navTop = $("#pistacia_mainNav ul").position();
	var servicesTop = $(".pistacia_serviceContainer").position();
	var aboutTop = $("#pistacia_aboutUsContainer").position();
	var staffTop = $("#pistacia_ourStaffContainer").position();
	var galleryTop = $("#pistacia_galleryContainer").position();
	var contactTop = $(".pistacia_contactUsContainer").position();

    var flag = navTop.top;

    $(function(){
	  $(window).scroll(function(){
	  	var currTop = $(window).scrollTop();
	  	if(currTop >= flag){
	  		//$("#pistacia_mainNav ul").css("background", "#B8D94C");
	  		$("#pistacia_mainNav ul").css("background", "#73AA2A");
	  		//10px solid #73AA2A

	  		$("#pistacia_mainNav li a").hover(function(){
			    $(this).css("color", "white");
			    }, function(){
			});

	  		$("#pistacia_mainNav ul").css("border-bottom", "10px solid #B8D94C");
	  		$("#pistacia_mainNav ul").css("position", "fixed");
	  		$("#pistacia_mainNav ul").css("top", "0");
	  	}else if(currTop<flag){

	  		$("#pistacia_mainNav li a").hover(function(){
			    $(this).css("color", "#73AA2A");
			    }, function(){
			    $(this).css("color", "white");
			});

	  		$("#pistacia_mainNav ul").css("border-bottom", "10px solid #73AA2A");
	  		$("#pistacia_mainNav ul").css("background", "none");
	  		$("#pistacia_mainNav ul").css("position", "absolute");
	  		$("#pistacia_mainNav ul").css("top", flag);
	  	}
	  });	
	})

	$("#pistacia_mainNav ul li").on("click", function(){
	  //alert($(this).text());
	 	if($(this).text() == "SERVICES"){
	 		 $('html, body').animate({
		        scrollTop: servicesTop.top
		    }, 1000);
	 	}else if($(this).text() == "HOME"){
	 		$('html, body').animate({
		        scrollTop: 0
		    }, 1000);
	 	}
	 	else if($(this).text() == "ABOUTUS"){
	 		$('html, body').animate({
		        scrollTop: aboutTop.top
		    }, 1000);
		    console.log(aboutTop.top);
	 	}
	 	else if($(this).text() == "OURSTAFF"){
	 		$('html, body').animate({
		        scrollTop: staffTop.top -60
		    }, 1000);
	 	}
	 	else if($(this).text() == "GALLERY"){
	 		$('html, body').animate({
		        scrollTop: galleryTop.top -60
		    }, 1000);
		    console.log("sulod");
	 	}
	 	else if($(this).text() == "CONTACT US"){
	 		$('html, body').animate({
		        scrollTop: contactTop.top -40
		    }, 1000);
	 	}
	});
});

